#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
ZetCode PyQt4 tutorial 

This program creates a menubar. The
menubar has one menu with an exit action.

author: Jan Bodnar
website: zetcode.com 
last edited: August 2011
"""

import sys
from PyQt4 import QtGui


class Example(QtGui.QMainWindow):

	def __init__(self):
		super(Example, self).__init__()

		self.initUI()
		
		
	def initUI(self):               
        
		exitAction = QtGui.QAction(QtGui.QIcon('exit.png'), '&Exit', self)        
		exitAction.setShortcut('Ctrl+Q')
		exitAction.setStatusTip('Exit')
		exitAction.triggered.connect(QtGui.qApp.quit)

		self.statusBar()

		menubar = self.menuBar()
		fileMenu = menubar.addMenu('&Caralho')
		fileMenu.addAction(exitAction)

		self.resize(250, 150)
		self.center() 
		self.setWindowTitle('Statusbar')
		self.setWindowIcon(QtGui.QIcon('python_icon.png'))    

		self.show()


	def center(self):

		qr = self.frameGeometry()
		cp = QtGui.QDesktopWidget().availableGeometry().center()
		qr.moveCenter(cp)
		self.move(qr.topLeft())	
        
        
def main():
    
    app = QtGui.QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main() 
